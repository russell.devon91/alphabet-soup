
import classes.CrossWord;
import classes.FileData;
import utils.FileUtil;

public class Main {
	public static void main(String[] args) {
		FileUtil fileUtil = new FileUtil();
		FileData fileData = fileUtil.readFileData(args[0]);
		CrossWord crossWord = new CrossWord(fileData);
		crossWord.printAnswerKey();
	}
}






