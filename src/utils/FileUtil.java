package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import classes.Coord;
import classes.FileData;

public class FileUtil {
	
	public FileUtil() {
		
	}
	
	public FileData readFileData(String fileName) {
		FileData fileData = new FileData();

		try {
		File myObj = new File(fileName);
		Scanner myReader = new Scanner(myObj);

		// get row and col numbers
		String[] tempStrArr = myReader.nextLine().split("x");
		fileData.setRow(Integer.parseInt(tempStrArr[0]));
		fileData.setCol(Integer.parseInt(tempStrArr[1]));

		// get the grid of the crossword using given number of rows
		char tempChar;
		char[][] grid = new char[fileData.getRow()][fileData.getCol()];
		HashMap<Character, ArrayList<Coord>> coordMap = new HashMap<Character, ArrayList<Coord>>();
		for (int i = 0; i < fileData.getRow(); i++){
			tempStrArr = myReader.nextLine().split(" ");
			for (int j = 0; j < fileData.getCol(); j++) {
				tempChar = tempStrArr[j].charAt(0);

				if (!coordMap.containsKey(tempChar)) {
					coordMap.put(tempChar, new ArrayList<Coord>());
				}
				coordMap.get(tempChar).add(new Coord(i, j));
				grid[i][j] = tempChar;
			}
		}
		fileData.setGrid(grid);
		fileData.setCoordMap(coordMap);

		// read words to find
		List<String> words = new ArrayList<String>();
		while (myReader.hasNextLine()) {
			words.add(myReader.nextLine());
		}
		fileData.setWords(words);

		myReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return fileData;
	}
}
