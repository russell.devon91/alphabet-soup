package classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FileData {
	int row;
	int col;
	char[][] grid;
	List<String> words;
	HashMap<Character, ArrayList<Coord>> coordMap;
	
	public FileData() {
		words = new ArrayList<String>();
		coordMap = new HashMap<Character, ArrayList<Coord>>();
	}

	public int getRow() {
		return row;
	}
	
	public void setRow(int row) {
		this.row = row;
	}
	
	public int getCol() {
		return col;
	}
	
	public void setCol(int col) {
		this.col = col;
	}
	
	public char[][] getGrid() {
		return grid;
	}
	
	public void setGrid(char[][] grid) {
		this.grid = grid;
	}
	
	
	public List<String> getWords() {
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

	public HashMap<Character, ArrayList<Coord>> getCoordMap() {
		return coordMap;
	}
	
	public void setCoordMap(HashMap<Character, ArrayList<Coord>> coordMap) {
		this.coordMap = coordMap;
	}
	
}
