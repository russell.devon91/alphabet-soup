package classes;

public class CrossWord {
	FileData fileData;
	
	public CrossWord(FileData fileData) {
		this.fileData = fileData;
	}
	
	public void printAnswerKey() {
		for (String word : fileData.getWords()) {
			System.out.println(findWordCoords(word));
		}
	}

	private String findWordCoords(String word) {
		String tempStr;
		for (Coord start : fileData.getCoordMap().get(word.charAt(0))) {
			for (Coord end : fileData.getCoordMap().get(word.charAt(word.length() - 1))) {
				tempStr = checkWordCoords(word.replaceAll("\\s+", ""), start, end);
				if (tempStr != null) {
					return word + " " + start.toString() + " " + end.toString();
				}
			}
		}

		// the word is not found which should only happen for incorrect input which we can assume won't happen
		return null;
	}

	private String checkWordCoords(String word, Coord start, Coord end) {
		int rowInc = 0;
		int colInc = 0;
		
		if (start.getRow() == end.getRow() && Math.abs(start.getCol() - end.getCol()) + 1 == word.length()) { // horizontal word
			colInc = start.getCol() < end.getCol() ? 1 : -1;
		} else if (start.getCol() == end.getCol() && Math.abs(start.getRow() - end.getRow()) + 1 == word.length()) { // vertical word
			rowInc = start.getRow() < end.getRow() ? 1 : -1;
		} else if (Math.abs(start.getRow() - end.getRow()) == Math.abs(start.getCol() - end.getCol()) &&
			   Math.abs(start.getRow() - end.getRow()) + 1 == word.length()) { // diagonal word
			rowInc = start.getRow() < end.getRow() ? 1 : -1;
			colInc = start.getCol() < end.getCol() ? 1 : -1;
		}

		// use increment values to see if start and end coordinates are valid for a word
		if (rowInc == 0 && colInc == 0) {
			return null;
		}

		int rowInd = start.getRow();
		int colInd = start.getCol();
		for (char letter : word.toCharArray()) {
			if (fileData.getGrid()[rowInd][colInd] != letter) {
				return null;
			}
			
			rowInd += rowInc;
			colInd += colInc;
		}
		
		return word + " " + start.toString() + " " + end.toString();
	}
}
