package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import classes.FileData;
import utils.FileUtil;

class FileUtilTest {
	
	@Test
	void readFileDataTest() {	
		FileUtil fileUtil = new FileUtil();
		FileData fileData = fileUtil.readFileData("test/resources/crossWord3x3.txt");
		
		assertEquals(fileData.getCol(), 3);
		assertEquals(fileData.getRow(), 3);
		assertEquals(fileData.getWords().get(0), "ABC");
		assertEquals(fileData.getCoordMap().get('A').get(0).toString(), "0:0");
	}
}
