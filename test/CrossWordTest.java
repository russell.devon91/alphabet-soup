package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import classes.CrossWord;
import classes.FileData;
import utils.FileUtil;

class CrossWordTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	
	@BeforeEach
	public void setUpStream() {
	    System.setOut(new PrintStream(outContent));
	}

	@AfterEach
	public void restoreStream() {
	    System.setOut(originalOut);
	}
	
	@Test
	void checkWordCoordsTest() {
		FileUtil fileUtil = new FileUtil();
		FileData fileData = fileUtil.readFileData("test/resources/crossWord5x5.txt");
		CrossWord crossWord = new CrossWord(fileData);
		
		crossWord.printAnswerKey();
		assertEquals(outContent.toString(), "HELLO 0:0 4:4\n"
										  + "GOOD 4:0 4:3\n"
				                          + "BYE 1:3 1:1\n");
	}

}
